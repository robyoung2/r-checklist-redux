/* Takes all reducers and combines them in one object */
import {combineReducers} from 'redux';
import UserReducer from './reducer-users';

const allReducers = combineReducers({
    users: UserReducer /* Picks up all the objects from the reducer-users file */
});

/* We need to say what we are sending out from a file */
export default allReducers;


