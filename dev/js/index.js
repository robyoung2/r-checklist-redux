import 'babel-polyfill'; /* makes file compatible with older browsers */
import React from 'react';
import ReactDOM from "react-dom";
import {createStore} from 'redux'; /* Curly braces allow you to import code in one big function */
import {Provider} from 'react-redux';
import allReducers from './reducers';
import App from './components/app'

/* We create our main store which will be storing all of our application's data */
const store = createStore(allReducers); /* Store will be changing but we are not changing it in code */


ReactDOM.render(<Provider store={store}>    
        <App />
    </Provider>, document.getElementById('root'));
