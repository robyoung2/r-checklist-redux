import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

class UserList extends Component {

    createListItems() {
        return this.props.users.map((user) => {
            return (
               <li key={user.id}>{user.first}{user.last}</li>
            );
        });
        /* Equal to all data in reducer-users.js */
    }

    render() {
        return (
            <ul>
               {this.createListItems()}
            </ul>
        );
    }
}

function mapStateToProps(state) {
    return {
        users: state.users
        /* Takes a piece of state and sends it into your component as props */
    };
}

export default connect(mapStateToProps)(UserList);
/* Smart component or container */